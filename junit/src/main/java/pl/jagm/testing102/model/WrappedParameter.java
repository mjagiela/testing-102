package pl.jagm.testing102.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class WrappedParameter {

    @Getter
    private final String parameter;

}
