package pl.jagm.testing102.repository;

import lombok.AllArgsConstructor;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

@AllArgsConstructor
public class UserRepository {

    private final String apiUrl;
    private final String authToken;

    public long createUser(String userName) throws IOException {
        HttpPost post = new HttpPost(apiUrl + "/user");
        post.setHeader("Auth-token", authToken);
        post.setEntity(new StringEntity(userName));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

            return Long.parseLong(EntityUtils.toString(response.getEntity()));
        }
    }

}
