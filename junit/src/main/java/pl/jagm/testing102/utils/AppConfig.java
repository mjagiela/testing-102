package pl.jagm.testing102.utils;

public class AppConfig {

    private static boolean initialized = false;

    public static void init() {
        System.out.println("Init of the App");
        initialized = true;
    }

    public static void destroy() {
        initialized = false;
        System.out.println("App destroyed!");
    }

    public static boolean isInitialized() {
        return initialized;
    }

}
