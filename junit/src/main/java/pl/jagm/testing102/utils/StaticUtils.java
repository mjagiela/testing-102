package pl.jagm.testing102.utils;

public class StaticUtils {

    private StaticUtils() {
    }

    public static String decorateString(String parameter) {
        return "[" + parameter + "]";
    }

}
