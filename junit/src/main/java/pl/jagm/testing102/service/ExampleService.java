package pl.jagm.testing102.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import pl.jagm.testing102.model.WrappedParameter;
import pl.jagm.testing102.utils.StaticUtils;

@AllArgsConstructor
@NoArgsConstructor
public class ExampleService {

    private Manager manager;

    public void wrapParameter(String parameter) {
        manager.manage(new WrappedParameter(parameter));
    }

    public String decorate(String parameter) {
        if (shouldCallManager(parameter)) {
            return StaticUtils.decorateString(manager.returnSomething("--" + parameter + "--"));
        } else {
            return "000";
        }
    }

    private boolean shouldCallManager(String parameter) {
        return !"000".equals(parameter);
    }

}
