package pl.jagm.testing102.service;

import pl.jagm.testing102.model.WrappedParameter;

public interface Manager {

    void manage(WrappedParameter parameter);

    String returnSomething(String parameter);

}
