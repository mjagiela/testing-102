package pl.jagm.testing102.powermock;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.jagm.testing102.service.ExampleService;
import pl.jagm.testing102.service.Manager;
import pl.jagm.testing102.utils.StaticUtils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MockitoStaticExample {

    @Mock
    Manager manager;

    @InjectMocks
    ExampleService exampleService;

    @Test
    public void testWithoutMock() {
        when(manager.returnSomething("--abc--")).thenReturn("def");

        String result = exampleService.decorate("abc");

        assertEquals("[def]", result);
    }

    @Test
    @Ignore("Doesn't work without mockito-inline. And mockito-inline breaks PowerMock")
    public void testStatic() {
        when(manager.returnSomething("--abc--")).thenReturn("def");
        try (MockedStatic<StaticUtils> mocked = Mockito.mockStatic(StaticUtils.class)) {
            //noinspection ResultOfMethodCallIgnored
            mocked.when(() -> StaticUtils.decorateString("def")).thenReturn("xyz");

            String result = exampleService.decorate("abc");

            assertEquals("xyz", result);
        }
    }

}
