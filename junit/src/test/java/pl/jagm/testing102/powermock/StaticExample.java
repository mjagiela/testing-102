package pl.jagm.testing102.powermock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.jagm.testing102.service.ExampleService;
import pl.jagm.testing102.service.Manager;
import pl.jagm.testing102.utils.StaticUtils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StaticUtils.class)
public class StaticExample {

    @Mock
    Manager manager;

    @InjectMocks
    ExampleService exampleService;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testWithoutMock() {
        when(manager.returnSomething("--abc--")).thenReturn("def");

        String result = exampleService.decorate("abc");

        assertEquals("[def]", result);
    }

    @Test
    public void testStatic() {
        when(manager.returnSomething("--abc--")).thenReturn("def");
        PowerMockito.mockStatic(StaticUtils.class);
        PowerMockito.when(StaticUtils.decorateString("def")).thenReturn("xyz");

        String result = exampleService.decorate("abc");

        assertEquals("xyz", result);
    }

}
