package pl.jagm.testing102.powermock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.jagm.testing102.service.ExampleService;
import pl.jagm.testing102.service.Manager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ExampleService.class)
public class PrivateMethodExample {

    @Mock
    Manager manager;

    @InjectMocks
    ExampleService exampleService;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testWithoutMocking() {
        String result = exampleService.decorate("000");

        assertEquals("000", result);
    }

    @Test
    public void mockPrivateMethod() throws Exception {
        when(manager.returnSomething("--000--")).thenReturn("888");
        ExampleService underTest = PowerMockito.spy(exampleService);
        PowerMockito.doReturn(true).when(underTest, "shouldCallManager", "000");

        String result = underTest.decorate("000");

        assertEquals("[888]", result);
    }

}
