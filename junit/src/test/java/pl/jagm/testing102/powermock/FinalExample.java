package pl.jagm.testing102.powermock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FinalExample.FinalClass.class, FinalExample.FinalMethod.class})
public class FinalExample {

    @Test
    public void shouldMockFinalClass() {
        FinalClass mock = mock(FinalClass.class);
        when(mock.returnSeven()).thenReturn(102);

        assertEquals(102, mock.returnSeven());
    }

    @Test
    public void shouldMockFinalMethod() {
        FinalMethod mock = PowerMockito.mock(FinalMethod.class); // it has to be PowerMockito.mock()
        when(mock.returnSeven()).thenReturn(123);

        assertEquals(123, mock.returnSeven());
    }

    public static final class FinalClass {

        int returnSeven() {
            return 42;
        }

    }

    public static class FinalMethod {

        final int returnSeven() {
            return 42;
        }

    }

}
