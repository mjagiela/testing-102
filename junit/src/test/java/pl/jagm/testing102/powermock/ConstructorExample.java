package pl.jagm.testing102.powermock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.jagm.testing102.exception.MyFunnyException;
import pl.jagm.testing102.model.WrappedParameter;
import pl.jagm.testing102.service.ExampleService;

import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ExampleService.class) // important - class that creates a new object
public class ConstructorExample {

    ExampleService exampleService = new ExampleService(null);

    @Test(expected = MyFunnyException.class)
    public void testConstructor() throws Exception {
        whenNew(WrappedParameter.class).withArguments("12345").thenThrow(new MyFunnyException());

        exampleService.wrapParameter("12345");
    }

}
