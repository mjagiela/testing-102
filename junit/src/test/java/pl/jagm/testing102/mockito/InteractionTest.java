package pl.jagm.testing102.mockito;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.jagm.testing102.service.ExampleService;
import pl.jagm.testing102.service.Manager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/*
 * Mockito doesn't need to have all interactions defined. It will just return null. EasyMock and jMock require
 * all interactions to be defined.
 */
@RunWith(MockitoJUnitRunner.class)
public class InteractionTest {

    @Mock
    Manager mockedObject;

    @InjectMocks
    ExampleService underTest;

    @Test
    public void simpleVerification() {
        when(mockedObject.returnSomething("--abc--")).thenReturn("xyz");

        String result = underTest.decorate("abc");

        assertEquals("[xyz]", result);
        verify(mockedObject, times(1)).returnSomething("--abc--");
        verifyNoMoreInteractions(mockedObject);
    }

    @Test
    public void noInteraction() {
        String result = underTest.decorate("000");

        assertEquals("000", result);
        verifyNoInteractions(mockedObject);
    }

    @Test
    public void mockReset() {
        when(mockedObject.returnSomething("--abc--")).thenReturn("xyz");

        String result = underTest.decorate("abc");
        assertEquals("[xyz]", result);

        result = underTest.decorate("abc");
        assertEquals("[xyz]", result);

        reset(mockedObject);

        result = underTest.decorate("abc");
        assertEquals("[null]", result);
    }

    @Test
    public void mockMultipleReturns() {
        when(mockedObject.returnSomething("--abc--")).thenReturn("xyz", "uuu", "123");

        String result = underTest.decorate("abc");
        assertEquals("[xyz]", result);

        result = underTest.decorate("abc");
        assertEquals("[uuu]", result);

        result = underTest.decorate("abc");
        assertEquals("[123]", result);
    }

}
