package pl.jagm.testing102.mockito;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.jagm.testing102.model.WrappedParameter;
import pl.jagm.testing102.service.ExampleService;
import pl.jagm.testing102.service.Manager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ArgumentCaptorTest {

    @Mock
    Manager manager;

    @InjectMocks
    ExampleService underTest;

    @Test
    public void argumentCaptorTest() {
        underTest.wrapParameter("abc");

        ArgumentCaptor<WrappedParameter> argumentCaptor = ArgumentCaptor.forClass(WrappedParameter.class);
        verify(manager).manage(argumentCaptor.capture());

        WrappedParameter result = argumentCaptor.getValue();
        assertEquals("abc", result.getParameter());
    }

}
