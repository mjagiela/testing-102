package pl.jagm.testing102.mockito;

import org.junit.Test;

import static org.mockito.Mockito.*;

public class SpyTest {

    @Test
    public void spyTest() {
        Manager manager = new Manager();
        Manager spyManager = spy(manager);

        spyManager.manage("abc");
        spyManager.manage("abc");
        spyManager.manage("def");

        verify(spyManager, times(2)).manage("abc");
        verify(spyManager, times(1)).manage("def");
        verifyNoMoreInteractions(spyManager);
    }

    private static class Manager {

        @SuppressWarnings("unused")
        void manage(String parameter) {
        }

    }

}
