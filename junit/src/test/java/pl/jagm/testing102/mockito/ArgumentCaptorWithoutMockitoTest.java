package pl.jagm.testing102.mockito;

import lombok.Getter;
import org.junit.Before;
import org.junit.Test;
import pl.jagm.testing102.model.WrappedParameter;
import pl.jagm.testing102.service.ExampleService;
import pl.jagm.testing102.service.Manager;

import static org.junit.Assert.assertEquals;

public class ArgumentCaptorWithoutMockitoTest {

    private MockedManager manager;
    private ExampleService underTest;

    @Before
    public void setUp() {
        manager = new MockedManager();
        underTest = new ExampleService(manager);
    }

    @Test
    public void argumentCaptorTest() {
        underTest.wrapParameter("abc");

        assertEquals("abc", manager.getParameter().getParameter());
    }

    private static class MockedManager implements Manager {

        @Getter
        private WrappedParameter parameter;

        @Override
        public void manage(WrappedParameter parameter) {
            this.parameter = parameter;
        }

        @Override
        public String returnSomething(String parameter) {
            return parameter;
        }

    }

}
