package pl.jagm.testing102.mockito;

import org.junit.Test;
import pl.jagm.testing102.exception.MyFunnyException;
import pl.jagm.testing102.model.WrappedParameter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExceptionTest {

    @Test(expected = MyFunnyException.class)
    public void exception() {
        WrappedParameter mockedWrappedParameter = mock(WrappedParameter.class);
        when(mockedWrappedParameter.getParameter()).thenThrow(new MyFunnyException());

        //noinspection ResultOfMethodCallIgnored
        mockedWrappedParameter.getParameter();
    }

}
