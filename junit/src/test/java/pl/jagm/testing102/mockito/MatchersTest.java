package pl.jagm.testing102.mockito;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class MatchersTest {

    @Mock
    Example mockedObject;

    @Before
    public void setUp() {
        // MockitoAnnotations.initMocks(this); // deprecated
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void noMatchers() {
        when(mockedObject.doSomething("abc", 123, true)).thenReturn("xyz");

        assertEquals("xyz", mockedObject.doSomething("abc", 123, true));
        assertNull(mockedObject.doSomething("abcd", 123, true));
        assertNull(mockedObject.doSomething("abc", 1234, true));
        assertNull(mockedObject.doSomething("abc", 123, false));
    }

    @Test
    public void matchers() {
        // when(mockedObject.doSomething("abc", anyInt(), true)).thenReturn("xyz"); // would fail since we need all parameters to be matchers
        when(mockedObject.doSomething(eq("abc"), anyInt(), eq(true))).thenReturn("xyz");

        assertEquals("xyz", mockedObject.doSomething("abc", 123, true));
        assertEquals("xyz", mockedObject.doSomething("abc", 1234, true));
        assertNull(mockedObject.doSomething("abcd", 123, true));
        assertNull(mockedObject.doSomething("abc", 123, false));
    }

    @Test
    public void sameMatcher() {
        Integer integerParameter = 98765;
        when(mockedObject.doSomething(anyString(), same(integerParameter), anyBoolean())).thenReturn("xyz");

        assertEquals("xyz", mockedObject.doSomething("abc", integerParameter, true));
        assertNull(mockedObject.doSomething("abc", 98765, true));
    }

    interface Example {

        String doSomething(String parameter1, Integer parameter2, boolean parameter3);

    }

}
