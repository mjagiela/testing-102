package pl.jagm.testing102.matchers;

import lombok.RequiredArgsConstructor;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Map;

@RequiredArgsConstructor
public class BetterLoginAndPasswordMatcher extends TypeSafeDiagnosingMatcher<Map<String, String>> {

    private static final String LOGIN_KEY = "login";
    private static final String PASSWORD_KEY = "password";
    private final String login;
    private final String password;

    public static BetterLoginAndPasswordMatcher hasCorrectLoginAndPassword(String login, String password) {
        return new BetterLoginAndPasswordMatcher(login, password);
    }

    @Override
    protected boolean matchesSafely(Map<String, String> stringStringMap, Description description) {
        if (!stringStringMap.containsKey(LOGIN_KEY)) {
            description.appendText("does not contain login");
            return false;
        }

        if (!stringStringMap.containsKey(PASSWORD_KEY)) {
            description.appendText("does not contain password");
            return false;
        }

        if (!login.equals(stringStringMap.get(LOGIN_KEY))) {
            description.appendText("login \"" + stringStringMap.get(LOGIN_KEY) + "\" is incorrect");
            return false;
        }

        if (!password.equals(stringStringMap.get(PASSWORD_KEY))) {
            description.appendText("password \"" + stringStringMap.get(PASSWORD_KEY) + "\" is incorrect");
            return false;
        }
        return true;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("map with correct login and password");
    }

}
