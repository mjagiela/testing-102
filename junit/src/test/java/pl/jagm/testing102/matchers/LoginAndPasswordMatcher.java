package pl.jagm.testing102.matchers;

import lombok.RequiredArgsConstructor;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.Map;

@RequiredArgsConstructor
public class LoginAndPasswordMatcher extends TypeSafeMatcher<Map<String, String>> {

    private static final String LOGIN_KEY = "login";
    private static final String PASSWORD_KEY = "password";
    private final String login;
    private final String password;

    public static LoginAndPasswordMatcher hasLoginAndPassword(String login, String password) {
        return new LoginAndPasswordMatcher(login, password);
    }

    @Override
    protected boolean matchesSafely(Map<String, String> stringStringMap) {
        return stringStringMap.containsKey(LOGIN_KEY) &&
                stringStringMap.containsKey(PASSWORD_KEY) &&
                login.equals(stringStringMap.get(LOGIN_KEY)) &&
                password.equals(stringStringMap.get(PASSWORD_KEY));
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("map with correct login and password");
    }

}
