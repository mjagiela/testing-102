package pl.jagm.testing102.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class UltimateNumberMatcher extends TypeSafeMatcher<Integer> {

    private static final Integer ULTIMATE_NUMBER = 42;
    private static final UltimateNumberMatcher MATCHER_INSTANCE = new UltimateNumberMatcher();

    private UltimateNumberMatcher() {
    }

    public static Matcher<Integer> isUltimateNumber() {
        return MATCHER_INSTANCE;
    }

    @Override
    protected boolean matchesSafely(Integer integer) {
        return ULTIMATE_NUMBER.equals(integer);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("is ultimate number");
    }

}