package pl.jagm.testing102.wiremock;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;
import pl.jagm.testing102.repository.UserRepository;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

public class HttpClientTest {

    private static final int PORT = 8089;
    private static final String AUTH_TOKEN = "VerySecretPassword";

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(PORT); // it's better to use maven plugin to generate random and available port

    UserRepository userRepository = new UserRepository("http://localhost:" + PORT, AUTH_TOKEN);

    @Test
    public void shouldCreateUserAndReturnItsId() throws IOException {
        stubFor(post(urlEqualTo("/user"))
                .withRequestBody(matching("admin"))
                .withHeader("Auth-token", equalTo(AUTH_TOKEN))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("198")));

        long userId = userRepository.createUser("admin");

        assertEquals(198, userId);
        verify(postRequestedFor(urlMatching("/user"))
                .withHeader("Auth-token", equalTo(AUTH_TOKEN))
                .withRequestBody(matching("admin")));
    }

}
