package pl.jagm.testing102.rules;

import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runners.MethodSorters;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TempFolderTest {

    @Rule
    public TemporaryFolder tmpFolder = TemporaryFolder.builder().assureDeletion().build(); // since junit 4.13

    @ClassRule
    public static TemporaryFolder classTmpFolder = TemporaryFolder.builder().assureDeletion().build(); // since junit 4.13

    @Test
    public void test_1_TmpFolder() throws IOException {
        tmpFolder.newFolder("testFolder");
        tmpFolder.newFile("testFolder/abc.txt");

        File file = Paths.get(tmpFolder.getRoot().getAbsolutePath(), "testFolder", "abc.txt").toFile();

        assertTrue(file.exists());
    }

    @Test
    public void test_2_TmpFolderDoesntExistAnymore() {
        File file = Paths.get(tmpFolder.getRoot().getAbsolutePath(), "testFolder", "abc.txt").toFile();

        assertFalse(file.exists());
    }

    @Test
    public void test_3_classTmpFolder() throws IOException {
        classTmpFolder.newFolder("testFolder");
        classTmpFolder.newFile("testFolder/abc.txt");

        File file = Paths.get(classTmpFolder.getRoot().getAbsolutePath(), "testFolder", "abc.txt").toFile();

        assertTrue(file.exists());
    }

    @Test
    public void test_4_classTmpFolderDoesntExistAnymore() {
        File file = Paths.get(classTmpFolder.getRoot().getAbsolutePath(), "testFolder", "abc.txt").toFile();

        assertTrue(file.exists());
    }

}
