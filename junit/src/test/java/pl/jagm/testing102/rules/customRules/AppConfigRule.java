package pl.jagm.testing102.rules.customRules;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import pl.jagm.testing102.utils.AppConfig;

public class AppConfigRule implements TestRule {

    @Override
    public Statement apply(Statement statement, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    AppConfig.init();
                    statement.evaluate();
                } finally {
                    AppConfig.destroy();
                }
            }
        };
    }

}
