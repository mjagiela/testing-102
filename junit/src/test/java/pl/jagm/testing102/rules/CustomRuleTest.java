package pl.jagm.testing102.rules;

import org.junit.ClassRule;
import org.junit.Test;
import pl.jagm.testing102.rules.customRules.AppConfigRule;
import pl.jagm.testing102.utils.AppConfig;

import static org.junit.Assert.assertTrue;

public class CustomRuleTest {

    @ClassRule
    public static AppConfigRule appConfigRule = new AppConfigRule();

    @Test
    public void appIsInitialized() {
        assertTrue(AppConfig.isInitialized());
    }

    @Test
    public void anotherTest() {
        assertTrue(AppConfig.isInitialized());
    }

}
