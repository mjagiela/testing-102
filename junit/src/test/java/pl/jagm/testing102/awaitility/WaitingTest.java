package pl.jagm.testing102.awaitility;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.hamcrest.CoreMatchers.equalTo;

public class WaitingTest {

    @Test
    public void waitForResult() {
        LongComputationsCalculator calculator = new LongComputationsCalculator(1000);

        new Thread(calculator).start();

        await().atMost(2, TimeUnit.SECONDS).until(() -> calculator.getResult() == 42);
    }

    @Test
    public void waitForResultWithHamcrest() {
        LongComputationsCalculator calculator = new LongComputationsCalculator(1000);

        new Thread(calculator).start();

        await().atMost(2, TimeUnit.SECONDS).until(calculator::getResult, equalTo(42));
    }

    @Test
    @Ignore("Timeouts due to the sleep time")
    public void timeout() {
        LongComputationsCalculator calculator = new LongComputationsCalculator(5000);

        new Thread(calculator).start();

        await().atMost(2, TimeUnit.SECONDS).until(() -> calculator.getResult() == 42);
    }

    @Test
    @Ignore("Incorrect assertion")
    public void wrongAssertion() {
        LongComputationsCalculator calculator = new LongComputationsCalculator(1000);

        new Thread(calculator).start();

        await().atMost(2, TimeUnit.SECONDS).until(() -> calculator.getResult() == 5);
    }

    @RequiredArgsConstructor
    private static class LongComputationsCalculator implements Runnable {

        final int sleepTimeInMillis;

        @Getter
        int result;

        public void run() {
            sleep(sleepTimeInMillis);
            result = 42;
        }

        private void sleep(int sleepTimeInMillis) {
            try {
                Thread.sleep(sleepTimeInMillis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
