package pl.jagm.testing102.junit.parameters;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(DataProviderRunner.class)
public class ParametrizedExample2 {

    @Test
    @UseDataProvider("dataProviderMax")
    public void testMax(int a, int b, int expected) {
        int max = Math.max(a, b);

        assertEquals(expected, max);
    }

    @Test
    @DataProvider(value = {
            "1 | 2  | 1",
            "7 | 5  | 5",
            "5 | -7 | -7"
    }, splitBy = "\\|")
    public void testMin(int a, int b, int expected) {
        int min = Math.min(a, b);

        assertEquals(expected, min);
    }

    @DataProvider
    public static Object[][] dataProviderMax() {
        return new Object[][]{
                {1, 2, 2},
                {7, 5, 7},
                {5, -7, 5},
        };
    }

}
