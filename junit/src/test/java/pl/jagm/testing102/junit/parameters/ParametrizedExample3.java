package pl.jagm.testing102.junit.parameters;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class ParametrizedExample3 {

    @Test
    @Parameters({"1, 2, 2", "7, 5, 7", "-7, 5, 5"})
    public void testMax(int a, int b, int expected) {
        int max = Math.max(a, b);

        assertEquals(expected, max);
    }

    @Test
    @FileParameters("src/test/resources/test_max.csv")
    @TestCaseName("test max for {0}, {1}")
    public void testMax2(int a, int b, int expected) {
        int max = Math.max(a, b);

        assertEquals(expected, max);
    }

    @Test
    @Parameters(method = "parametersForMinTest")
    @TestCaseName("test min for {0}, {1}")
    public void testMin(int a, int b, int expected) {
        int min = Math.min(a, b);

        assertEquals(expected, min);
    }

    @SuppressWarnings("unused")
    private Object parametersForMinTest() {
        return new Object[][]{
                {1, 2, 1},
                {7, 5, 5},
                {-7, 5, -7}
        };
    }

}
