package pl.jagm.testing102.junit.parameters;

import lombok.AllArgsConstructor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
@AllArgsConstructor
public class ParametrizedExample1 {

    private final int a;
    private final int b;
    private final int expected;

    @Parameterized.Parameters()
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, 2, 1},
                {7, 5, 5},
                {5, -7, -7}
        });
    }

    @Test
    public void testMin() {
        int min = Math.min(a, b);

        assertEquals(expected, min);
    }

}
