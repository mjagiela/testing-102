package pl.jagm.testing102.easymock;

import org.easymock.EasyMockRunner;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.jagm.testing102.service.ExampleService;
import pl.jagm.testing102.service.Manager;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

@RunWith(EasyMockRunner.class)
public class EasyMockExample {

    @Mock
    Manager manager;

    @TestSubject
    ExampleService exampleService = new ExampleService();

    @Test
    public void interactionTest() {
        expect(manager.returnSomething("--abc--")).andReturn("def");
        expectLastCall().times(2);
        replay(manager);

        String result = exampleService.decorate("abc");
        assertEquals("[def]", result);

        result = exampleService.decorate("abc");
        assertEquals("[def]", result);

        verify(manager);
    }

    @Test
    public void noInteractionTest() {
        replay(manager);

        String result = exampleService.decorate("000");

        assertEquals("000", result);
        verify(manager);
    }
}
