package pl.jagm.testing102.hamcrest;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static pl.jagm.testing102.matchers.BetterLoginAndPasswordMatcher.hasCorrectLoginAndPassword;
import static pl.jagm.testing102.matchers.LoginAndPasswordMatcher.hasLoginAndPassword;
import static pl.jagm.testing102.matchers.UltimateNumberMatcher.isUltimateNumber;

public class Example {

    @Test
    public void simpleMatcher() {
        String a = "String";
        String b = "anotherString";

        assertThat(b, endsWith(a));
    }

    @Test
    public void collectionsMatcher() {
        final List<String> list = asList("abc", "def", "ghi", "jkl", "mno", "pqr");

        assertThat(list, hasItems("def", "mno"));
    }

    @Test
    public void testCustomMatcher() {
        assertThat(42, isUltimateNumber());
    }

    @Test
    public void testMapWithLoginAndPassword() {
        ImmutableMap<String, String> credentialsMap = ImmutableMap.<String, String>builder()
                .put("login", "admin")
                .put("password", "iddqd")
                .build();

        assertThat(credentialsMap, hasLoginAndPassword("admin", "iddqd"));
    }

    @Test
    public void testMapWithLoginAndPasswordButBetter() {
        ImmutableMap<String, String> credentialsMap = ImmutableMap.<String, String>builder()
                .put("login", "admin")
                .put("password", "iddqd")
                .build();

        assertThat(credentialsMap, hasCorrectLoginAndPassword("admin", "iddqd"));
    }

}
