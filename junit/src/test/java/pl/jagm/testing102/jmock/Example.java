package pl.jagm.testing102.jmock;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.States;
import org.junit.Before;
import org.junit.Test;
import pl.jagm.testing102.exception.MyFunnyException;
import pl.jagm.testing102.service.ExampleService;
import pl.jagm.testing102.service.Manager;

import static org.junit.Assert.assertEquals;

public class Example {

    Mockery context = new Mockery();
    Manager manager;
    ExampleService exampleService;

    @Before
    public void setUp() {
        manager = context.mock(Manager.class);
        exampleService = new ExampleService(manager);
    }

    @Test
    public void testInteraction() {
        context.checking(new Expectations() {{
            exactly(2).of(manager).returnSomething("--abc--");
            will(returnValue("def"));
        }});

        String result = exampleService.decorate("abc");
        assertEquals("[def]", result);

        result = exampleService.decorate("abc");
        assertEquals("[def]", result);

        context.assertIsSatisfied();
    }

    @Test
    public void testNoInteractions() {
        String result = exampleService.decorate("000");
        assertEquals("000", result);

        context.assertIsSatisfied();
    }

    @Test(expected = MyFunnyException.class)
    public void testException() {
        context.checking(new Expectations() {{
            oneOf(manager).returnSomething("--abc--");
            will(throwException(new MyFunnyException()));
        }});

        exampleService.decorate("abc");
    }

    @Test
    public void testState() {
        final States state = context.states("state").startsAs("start");

        context.checking(new Expectations() {{
            oneOf(manager).returnSomething("--move--");
            when(state.is("moved"));
            then(state.is("almost-done"));
            will(returnValue("almost done"));
            oneOf(manager).returnSomething("--move--");
            when(state.is("start"));
            then(state.is("moved"));
            will(returnValue("ok"));
            oneOf(manager).returnSomething("--finish--");
            when(state.is("almost-done"));
            will(returnValue("finished"));
        }});

        String result = exampleService.decorate("move");
        assertEquals("[ok]", result);

        result = exampleService.decorate("move");
        assertEquals("[almost done]", result);

        result = exampleService.decorate("finish");
        assertEquals("[finished]", result);

        context.assertIsSatisfied();
    }

}
