Testing 102
===========

Repository contains examples of tools and libraries that can be used for writing unit and integration tests


List of examples
----------------

1. Tools
    1. ✓ [TestNG](https://testng.org/)
    1. ✓ [Junit](https://junit.org/junit4/)
        1. ✓ Data driven tests - [#1](https://github.com/TNG/junit-dataprovider), [#2](https://github.com/Pragmatists/JUnitParams)
    1. [~~Spock~~](http://spockframework.org/) - it will be a separate presentation
    1. ✓ [Mockito](https://site.mockito.org/)
        1. ✓ Matchers
        1. ✓ Verifying
        1. ✓ Argument captor
        1. ✓ Spy
    1. ✓ Other mocking libraries
        1. ✓ [EasyMock](https://easymock.org/)
        1. ✓ [jMock](http://jmock.org/)
    1. ✓ [Powermock](https://powermock.github.io/)
        1. ✓ Static method
        1. ✓ Private method
        1. ✓ Final class/method
        1. ✓ Constructor
        1. ✓ Mockito
    1. ✓ Wiremock
        1. It allows also for recording requests/responses and then playback
    1. ✓ Rule and Class rule
        1. ✓ Library example
        1. ✓ Custom rule
    1. ✓ [Awaitility](https://github.com/awaitility/awaitility)
    1. ✓ Testing spring context
    1. ✓ [Hamcrest Matchers](http://hamcrest.org/JavaHamcrest/)
        1. ✓ Simple
        1. ✓ Advanced
    1. In-memory
        1. DynamoDB
        1. SQS
        1. S3 ?
1. Best practises
    1. Simple and small tests
    1. Readable
    1. Test only one thing
    1. Meaningful name
    1. Use custom matchers instead of long list of assertions
    1. Don't mock data objects
    1. Stable tests
    1. Easy to write (TDD)
    1. It's your documentation
1. Code smells
    1. Private methods
    1. Testing mocks
    1. Using PowerMock


Bibliography
------------
* [Mockito vs EasyMock vs JMockit](https://www.baeldung.com/mockito-vs-easymock-vs-jmockit)
* [Mocking Private, Static And Void Methods Using Mockito](https://www.softwaretestinghelp.com/mock-private-static-void-methods-mockito/)
* [PowerMockito Tutorial for Beginners](https://examples.javacodegeeks.com/core-java/powermockito/powermockito-tutorial-beginners/)
* [Mock the unmockable: opt-in mocking of final classes/methods](https://github.com/mockito/mockito/wiki/What%27s-new-in-Mockito-2#unmockable)
* [Custom Hamcrest Matchers](https://www.javacodegeeks.com/2015/11/custom-hamcrest-matchers.html)
* [Guide to JUnit 4 Rules](https://www.baeldung.com/junit-4-rules)