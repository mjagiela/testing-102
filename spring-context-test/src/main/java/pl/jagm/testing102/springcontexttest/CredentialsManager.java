package pl.jagm.testing102.springcontexttest;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class CredentialsManager {

    @Autowired
    private Provider credentialsProvider;

    private User user;

    public CredentialsManager() {
    }

    @PostConstruct
    public void createCredentials() {
        user = credentialsProvider.create();
    }

    public boolean hasAuthorizedUser() {
        return user != null && user.isAuthorized();
    }

}
