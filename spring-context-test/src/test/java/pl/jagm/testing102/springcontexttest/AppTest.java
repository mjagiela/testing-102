package pl.jagm.testing102.springcontexttest;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
        @ContextConfiguration(classes = AppTest.TestConfig.class),
        @ContextConfiguration("classpath:spring-context.xml")
})
public class AppTest {

    @Autowired
    CredentialsManager credentialsUser;

    @Test
    public void testContext() {
        assertTrue(credentialsUser.hasAuthorizedUser());
    }

    @Configuration
    @Log4j2
    public static class TestConfig {

        private static final boolean IS_AUTHORIZED = true;

        @Bean
        @Primary
        Provider usersProvider() {
            log.info("Creating new provider");
            return () -> {
                log.info("Create provider");
                return new User(IS_AUTHORIZED);
            };
        }

    }

}


