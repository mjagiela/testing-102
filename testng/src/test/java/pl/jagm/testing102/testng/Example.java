package pl.jagm.testing102.testng;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Example {

    @Test
    public void testMin() {
        int min = Math.min(7, 17);

        assertEquals(min, 7);
    }

    @Test(dataProvider = "testMax")
    public void testMax(int a, int b, int expectedResult) {
        int max = Math.max(a, b);

        assertEquals(max, expectedResult);
    }

    @DataProvider(name = "testMax")
    public static Object[][] testMaxDataProvider() {
        return new Object[][]{
                {1, 2, 2},
                {7, 2, 7},
                {1, -2, 1}
        };
    }

}
